<?php


namespace test;

use struct\Stack;

include '../src/Stack/Stack.php';

/**
 * Class StackTest
 * @package test
 * @author robotomize@gmail.com
 */
class StackTest extends \PHPUnit_Framework_TestCase
{
    public function testPushToStack()
    {
        $tt = new Stack(4);
        $tt->push(4);
        $this->assertEquals(4, $tt->pop());
    }

    public function testPopFromStack()
    {
        $tt = new Stack(4);
        $tt->push(10);
        $tt->push(20);
        $tt->push(30);
        $tt->push(40);
        $this->assertEquals(40, $tt->pop());
        $this->assertEquals(30, $tt->pop());
        $this->assertEquals(20, $tt->pop());
        $this->assertEquals(10, $tt->pop());
    }
}
