## PHP data structs ##
Created for educational purposes, to study the structure of the data
* Stack
* Fixed Array
* AvgMoreLessValue algorithm
* Fast binary search in 20min in PHP

### How do I get set up? ###

* git clone and usage

### Examples Stack###

```php
<?php

namespace struct;

include 'Stack.php';

$myBooks = new Stack();

$myBooks->push('Игра престолов');
$myBooks->push('99 франков');
$myBooks->push('Девушка с татуировкой дракона');
$myBooks->push('Цветы для Элджернона');

```

### AvgMoreLess algorithm ###
```php
<?php

namespace struct;

include 'AvgMoreLessValue.php';

$tt = new AvgMoreLessValue([0, 2, 3, 7, 10, 12, 1, 3, 6, 4, 5, 1, 3, 6]);
print $tt->getMoreLess() . PHP_EOL;
print sprintf('AVG value %s', $tt->getAvg()) . PHP_EOL;
print sprintf('Less average %s ', $tt->getLess()) . PHP_EOL;
print sprintf('More average %s ', $tt->getMore()) . PHP_EOL;


```


### The binary search in 20min ###
#### Simple binary search####
```php
<?php

namespace FiveMinBinarySearch;

use FiveMinBinarySearch\FiveMinBinarySearch;

include 'FiveMinBinarySearch.php';

$tt = new FiveMinBinarySearch('372363105623559049375267496732', 5); // matching 5
print $tt . PHP_EOL;
print $tt->start();

```

