<?php

namespace struct;

include 'Stack.php';

$myBooks = new Stack();

$myBooks->push('Игра престолов');
$myBooks->push('99 франков');
$myBooks->push('Девушка с татуировкой дракона');
$myBooks->push('Цветы для Элджернона');

foreach ($myBooks as $book) {
    print $book . PHP_EOL;
}