<?php

namespace struct;

/**
 * Class Stack
 * @package struct
 * @author robotomize@gmail.com
 * @usage
 * $tt = new Stack(10);
 * $tt->push(1);
 * print_r($tt);
 */
class Stack extends \SplStack
{
    /**
     * @var array
     */
    protected $stack;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @param int $limit
     */
    public function __construct($limit = 10)
    {
        $this->stack = [];
        $this->limit = $limit;
    }


    /**
     * Push element to stack
     * @param mixed $item
     * @throws \Exception
     */
    public function push($item)
    {
        if (count($this->stack) < $this->limit) {
            array_unshift($this->stack, $item); // code smells
        } else {
            throw new \Exception('Stack is full');
        }
    }


    /**
     * get last element from stack
     * @return mixed
     * @throws \Exception
     */
    public function pop()
    {
        if (empty($this->stack)) {
            throw new \Exception('Stack is empty');
        } else {
            return array_shift($this->stack);
        }
    }

    /**
     * Print current stack element
     * @return mixed
     */
    public function top() {
        return current($this->stack);
    }

    /**
     * Serialize my stack
     * @return string
     * @throws \Exception
     */
    public function __toString()
    {
        if (!empty($this->stack)) {
            return serialize($this->stack);
        } else {
            return '';
        }
        // TODO: Implement __toString() method.
    }

    /**
     * Dump my stack
     * @throws \Exception
     */
    public function __invoke()
    {
        if (!empty($this->stack)) {
            print_r($this->stack);
        } else {
            throw new \Exception('Stack is empty');
        }
        // TODO: Implement __invoke() method.
    }


    /**
     * not implemented
     */
    public function __clone()
    {
        // TODO: Implement __clone() method.
    }

    /**
     * not implemented
     */
    public function __wakeup()
    {
        // TODO: Implement __wakeup() method.
    }


}