<?php

namespace struct;

/**
 * Class Inserts
 * @package struct
 * @author robotomize@gmail.com
 */
class Inserts
{
    /**
     * @var array
     */
    private $input = [];

    /**
     * @param array $input
     */
    public function __construct($input)
    {
        if (is_array($input) && 0 !== count($input)) {
            $this->input = $input;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * @var bool
     */
    private $run = false;

    /**
     * Main sort method
     */
    public function run()
    {
        $this->run = true;
        for ($i = 1; $i < count($this->input); $i++) {
            $new = $this->input[$i];
            $loc = $i - 1;
            while ($loc >= 0 && $this->input[$loc] > $new) {
                $this->input[$loc + 1] = $this->input[$loc];
                $loc--;
            }
            $this->input[$loc + 1] = $new;
        }
    }

    /**
     * @return array
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @param array $input
     */
    public function setInput($input)
    {
        $this->input = $input;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (0 !== count($this->input) && $this->run === true) {
            return serialize($this->input);
        } else {
            $this->run();
            return (0 !== count($this->input)) ? serialize($this->input) : '';
        }
    }

    /**
     * @return array
     */
    public function __invoke()
    {
        if (0 !== count($this->input) && $this->run === true) {
            return $this->input;
        } else {
            $this->run();
            return $this->input;
        }
    }
}
