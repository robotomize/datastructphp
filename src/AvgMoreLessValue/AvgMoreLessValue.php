<?php

namespace struct;

/**
 * Class AvgMoreLessValue
 * @package struct
 * @author robotomzie@gmail.com
 */
class AvgMoreLessValue
{

    /**
     * @var float
     */
    private $_avg = 0.0;

    /**
     * @var array
     */
    private $_numbersArray = [];

    /**
     * @return array
     */
    public function getNumbersArray()
    {
        return $this->_numbersArray;
    }

    /**
     * @return float
     */
    public function getAvg()
    {
        return $this->_avg;
    }

    /**
     * @param $arrayNumbers
     */
    public function __construct($arrayNumbers)
    {
        $this->_numbersArray = $arrayNumbers;
    }

    /**
     * @return float
     */
    private function calculateAvg()
    {
        $sum = 0;

        foreach ($this->_numbersArray as $values) {
            $sum += $values;
        }

        $this->_avg = round($sum / count($this->_numbersArray), 1);

        return $this->_avg;
    }

    /**
     * @var int
     */
    private $_more = 0;

    /**
     * @return int
     */
    public function getMore()
    {
        return $this->_more;
    }

    /**
     * @var int
     */
    private $_less = 0;

    /**
     * @return int
     */
    public function getLess()
    {
        return $this->_less;
    }

    /**
     * @return string
     */
    public function getMoreLess()
    {
        $this->calculateAvg();

        foreach ($this->_numbersArray as $values) {
            if ($values > $this->_avg) {
                $this->_more++;
            } else {
                $this->_less++;
            }
        }

        if ($this->_more > $this->_less) {
            return 'The number of integers that are larger average';
        } else {
            return 'The number of integers which are smaller than the average value of greater';
        }
    }
}