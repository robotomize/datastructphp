<?php

namespace struct;

include 'AvgMoreLessValue.php';

$tt = new AvgMoreLessValue([0, 2, 3, 7, 10, 12, 1, 3, 6, 4, 5, 1, 3, 6]);
print $tt->getMoreLess() . PHP_EOL;
print sprintf('AVG value %s', $tt->getAvg()) . PHP_EOL;
print sprintf('Less average %s ', $tt->getLess()) . PHP_EOL;
print sprintf('More average %s ', $tt->getMore()) . PHP_EOL;
