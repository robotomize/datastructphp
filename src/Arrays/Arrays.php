<?php

$startMemory = memory_get_usage();
$array = range(1, 1000000);

$firstResult = sprintf('%s bytes', (memory_get_usage() - $startMemory));
unset($startMemory);

$startMemory = memory_get_usage();

$array = new SplFixedArray(1000000);
for ($i = 0; $i < 1000000; ++$i) {
    $array[$i] = $i;
}

$secondResult = sprintf('%s bytes', (memory_get_usage() - $startMemory));

print $firstResult . PHP_EOL;
print $secondResult . PHP_EOL;